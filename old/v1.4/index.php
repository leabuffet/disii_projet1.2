<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>HOME</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="css/style.css">
        <link rel="shortcut icon" type="img/png" href="img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a class="navActive" href="index.php">HOME</a></li>
                    <li><a href="views/apropos.php">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="views/gallery.php">GALLERY</a></li>
                    <li><a href="views/contact.php">CONTACT</a></li>
                    <li><a href="views/loginForm.php">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="./img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="./img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a class="navActive" href="index.php">HOME</a></li>
                    <li><a href="views/apropos.php">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="views/gallery.php">GALLERY</a></li>
                    <li><a href="views/contact.php">CONTACT</a></li>
                    <li><a href="views/profil.php">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section class="gauche">
                <div class="id">
                    <img id="logo" src="./img/LogoLB_wh.png" alt="logo">
                    <h1>Léa<br>Buffet</h1>
                </div>
                <div id="sousTitre">
                    <h2>GRAPHISTE</h2>
                </div>
                <div class="socialmedia">
                    <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
                    <a href="https://www.instagram.com/_l.bft/">instagram</a>
                </div>
            </section>
            <section class="slideshow-container">
                <div class="mySlides fade">
                    <img src="img/slide1.jpg" style="width:100%">
                </div>
                <div class="mySlides fade">
                    <img src="img/slide2.jpg" style="width:100%">
                </div>
                <div class="mySlides fade">
                    <img src="img/slide3.jpg" style="width:100%">
                </div>
            </section>
            <!-- RESPONSIVE -->
            <section class="main-res">
                <div id="imgBack-res">
                    <div id="idRes">
                        <h1>Léa<br>Buffet</h1>
                        <h2>GRAPHISTE</h2>
                    </div>
                </div>
            </section>
            <!-- ---------- -->
        </main>
        <?php 
        include('views/footer.php');
        ?>
</html>