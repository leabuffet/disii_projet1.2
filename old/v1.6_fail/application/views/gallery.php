<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>GALLERY</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../views/apropos.php">ABOUT</a></li>
                    <li><a href="../views/project.php">PROJECT</a></li>
                    <li><a class="navActive" href="../views/gallery.php">GALLERY</a></li>
                    <li><a href="../views/contact.php">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="../img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="../img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../views/apropos.php">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="../views/gallery.php">GALLERY</a></li>
                    <li><a class="navActive" href="../views/contact.php">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section id="gall-res">
                <div class="gallery">
                    <a class ="titre-gall" href="../views/redscale.php">110 - REDSCALE 2016 - 2018</a>
                    <a href="../views/redscale.php"><img src="../img/redscale_cover.jpg"/></a>
                </div>
                <div class="gallery">
                    <a class ="titre-gall" href="../views/loire.php">35 - LOIRE 2016 </a>
                    <a href="../views/loire.php"><img src="../img/canon_cover.jpg"></a>
                </div>
                <div class="gallery">
                    <a class ="titre-gall" href="../views/esad.php">35 - ESAD 2014 - 2015</a>
                    <a href="../views/esad.php"><img src="../img/minolta_cover.jpg"></a>
                </div>
            </section>
        </main>
    <?php 
    include('footer.php');
     ?>
</html>