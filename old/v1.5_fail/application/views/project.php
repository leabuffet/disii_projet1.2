<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>PROJECT</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="../index.html">HOME</a></li>
                    <li><a href="../html/apropos.html">ABOUT</a></li>
                    <li><a class="navActive" href="../html/project.html">PROJECT</a></li>
                    <li><a href="../html/gallery.html">GALLERY</a></li>
                    <li><a href="../html/contact.html">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <main>
            <section class="gaucheP">
                <div class="projetG">
                    <a class="titreProjet" href="../html/projet1.html">01 -<br>40&+</a>
                    <a href="../html/projet1.html"><img src="../img/40ans_cover.jpg"/></a>
                </div>
                <div class="projetG">
                    <a class="titreProjet" href="../html/projet2.html">02 -<br>CAMELLIA<br>SINENSIS</a>
                    <a href="../html/projet2.html"><img src="../img/sinensis_cover.jpg"/></a>
                </div>
                <div class="projetG">
                    <a class="titreProjet" href="../html/projet3.html">03 -<br>DÉFORMATION</a>
                    <a href="../html/projet3.html"><img src="../img/jeu_cover.jpg"/></a>
                </div>
            </section>
            <section class="droiteP">
                <div class="projetD">
                    <a class="titreProjet" href="../html/projet4.html">- 04<br>ABANDONNÉE</a>
                    <a href="../html/projet4.html"><img src="../img/frida_cover.jpg"/></a>
                </div>
                 <div class="projetD">
                    <a class="titreProjet" href="../html/projet5.html">- 05<br>PERDU</a>
                    <a href="../html/projet5.html"><img src="../img/perdu_cover.jpg"/></a>
                </div>
                <div class="projetD">
                    <a class="titreProjet" href="../html/projet6.html">- 06<br>PORTRAIT</a>
                    <a href="../html/projet6.html"><img src="../img/portraits_cover.jpg"/></a>
                </div>
            </section>
        </main>
        <footer>
            <div class="bas">
                <a href="../html/mentions.html">Mentions Légales</a>
                <p><a href="../html/plan.html">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <style type="text/css">
                .slideshow-container {
                    margin-right: auto;
                }
            </style>
        <script src="../JS/slideshow.js"></script>
    </body>
</html>