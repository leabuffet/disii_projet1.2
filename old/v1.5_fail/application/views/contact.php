<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>CONTACT</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../views/apropos.php">ABOUT</a></li>
                   <li><a href="../views/project.php">PROJECT</a></li>
                    <li><a href="../views/gallery.php">GALLERY</a></li>
                    <li><a class="navActive" href="../views/contact.php">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="../img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="../img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a href="../index.php">HOME</a></li>
                    <li><a href="../views/apropos.php">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="../views/gallery.php">GALLERY</a></li>
                    <li><a class="navActive" href="../views/contact.php">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section class="gauche">
                <div class="id">
                    <img id="logo" src="../img/LogoLB_wh.png" alt="logo">
                    <h1>Léa<br>Buffet</h1>
                </div>
                <div id="sousTitre">
                    <h2>GRAPHISTE</h2>
                </div>
                <div id="contactMain">
                    <div id="contact">
                        <h3>ÉCRIVEZ MOI:</h3>
                        <h3>OU</h3>
                        <h3>CONTACTEZ MOI:</h3>
                    </div>
                    <div id="info">
                        <a href="https://goo.gl/maps/cDPoqEymfnS2">6 rue Friesé<br>67000 Strasbourg</a>
                        <p>06.72.39.74.72<br>lbuffet5@gmail.com</p>
                    </div>
                    <div class="socialmedia">
                        <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
                        <a href="https://www.instagram.com/_l.bft/">instagram</a>
                    </div>
                    <form method="post" action="mail.php" id="form">
                        <div class="form-group">
                            <input type="nom" name="name" class="form-control" id="formNom" placeholder="nom, prénom">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" id="formMail" placeholder="email">
                        </div>
                        <div class="form-group">
                            <textarea name="message" class="form-control" id="FormText" rows="3" placeholder="votre message..."></textarea>
                        </div>
                        <div class="form-group">
                             <input id="btn-submit" type="submit" value="ENVOYER">
                        </div>
                    </form>
                </section>
                <section class="slideshow-container">
                    <div class="mySlides fade">
                        <img src="../img/slide1.jpg" style="width:100%">
                    </div>
                    <div class="mySlides fade">
                        <img src="../img/slide2.jpg" style="width:100%">
                    </div>
                    <div class="mySlides fade">
                        <img src="../img/slide3.jpg" style="width:100%">
                    </div>
                </section>
                <!-- RESPONSIVE -->
                <section class="main-res">
                    <div id="imgBack-res">
                        <div id="idRes">
                            <h1>Léa<br>Buffet</h1>
                            <h2>GRAPHISTE</h2>
                        </div>
                    </div>
                    <div id="contact-res">
                        <div id="info">
                            <a href="https://goo.gl/maps/cDPoqEymfnS2">6 rue Friesé<br>67000 Strasbourg</a>
                            <p>+33(0)6.72.39.74.72<br>lbuffet5@gmail.com</p>
                        </div>
                        <form id="form-Res">
                            <p>CONTACTEZ MOI :</p>
                            <div class="form-group" style="background: none;">
                                <input type="nom" class="form-control" id="formNom" placeholder="nom, prénom">
                            </div>
                            <div class="form-group" style="background: none;">
                                <input type="email" class="form-control" id="formMail" placeholder="email">
                            </div>
                            <div class="form-group" style="background: none;">
                                <textarea class="form-control" id="FormText" rows="3" placeholder="votre message..."></textarea>
                            </div>
                            <div class="form-group">
                             <input id="btn-submit" type="submit" value="ENVOYER">
                        </div>
                        </form>
                    </div>
                </section>
                <!-- ---------- -->
            </main>
            <footer>
                <div class="bas">
                    <a href="../views/mentions.php">Mentions Légales</a>
                    <p><a href="../views/plan.php">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
            <!-- RESPONSIVE -->
            <div class="socialmedia-resp">
                <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
                <a id="instagram"href="https://www.instagram.com/_l.bft/">instagram</a>
            </div>
            <footer id="footer-responsive" style="margin-top: 350px;">
                
                <div class="bas">
                    <a href="../views/mentions.php">Mentions Légales</a>
                    <a href="../views/plan.php">Plan du site</a>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
            <!-- ---------- -->
            <script src="../JS/slideshow.js"></script>
            <script src="../JS/menu.js"></script>
        </body>
    </html>