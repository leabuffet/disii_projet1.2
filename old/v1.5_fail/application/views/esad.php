<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>GALLERY - 35 ESAD</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
       <?php 
            include('header_gall.php');
        ?>
            <section>
                <div class="row">
                    <div class="column">
                        <a href="#img1"><img src="../img/esad/esad01.jpg"></a>
                        <a href="#img2"><img src="../img/esad/esad02.jpg"></a>
                        <a href="#img3"><img src="../img/esad/esad03.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img4"><img src="../img/esad/esad12.jpg"></a>
                        <a href="#img5"><img src="../img/esad/esad11.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img6"><img src="../img/esad/esad07.jpg"></a>
                        <a href="#img7"><img src="../img/esad/esad08.jpg"></a>
                        <a href="#img8"><img src="../img/esad/esad06.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img9"><img src="../img/esad/esad10.jpg"></a>
                        <a href="#img10"><img src="../img/esad/esad04.jpg"></a>
                    </div>
                </div>
                <div id="img-lightbox">
                    <a href="#_" class="lightbox" id="img1"><img src="../img/esad/esad01.jpg"></a>
                    <a href="#_" class="lightbox" id="img2"><img src="../img/esad/esad02.jpg"></a>
                    <a href="#_" class="lightbox" id="img3"><img src="../img/esad/esad03.jpg"></a>
                    <a href="#_" class="lightbox" id="img4"><img src="../img/esad/esad12.jpg"></a>
                    <a href="#_" class="lightbox" id="img5"><img src="../img/esad/esad11.jpg"></a>
                    <a href="#_" class="lightbox" id="img6"><img src="../img/esad/esad07.jpg"></a>
                    <a href="#_" class="lightbox" id="img7"><img src="../img/esad/esad08.jpg"></a>
                    <a href="#_" class="lightbox" id="img8"><img src="../img/esad/esad06.jpg"></a>
                    <a href="#_" class="lightbox" id="img9"><img src="../img/esad/esad10.jpg"></a>
                    <a href="#_" class="lightbox" id="img10"><img src="../img/esad/esad04.jpg"></a>
                </div>
            </section>
             <section>
                <div class="cont-btn-back">
                    <a class="btn-back" href="../views/gallery.php" >other gallery</a>
                </div>
            </section>
        </main>
        <?php 
        include('footer.php');
        ?>
</html>