<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>CONTACT</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="../index.html">HOME</a></li>
                    <li><a href="../html/apropos.html">ABOUT</a></li>
                   <li><a href="../html/project.html">PROJECT</a></li>
                    <li><a href="../html/gallery.html">GALLERY</a></li>
                    <li><a class="navActive" href="../html/contact.html">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="../img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="../img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a href="../index.html">HOME</a></li>
                    <li><a href="../html/apropos.html">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="../html/gallery.html">GALLERY</a></li>
                    <li><a class="navActive" href="../html/contact.html">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section class="gauche">
                <div class="id">
                    <h1>$user_first_name<br>$user_last_name</h1>
                </div>
                <div id="sousTitre">
                    <h2>$user_status</h2>
                </div>
                <div id="contactMain">
                    <div id="contact">
                        <h3>STATISTIQUES:</h3>
                        <h3>ET</h3>
                        <h3>INFOS PROFIL:</h3>
                    </div>
                    <div id="info">
                        <p>Adresse mail:</p><br><p>$user_mail</p>
                        <p>Téléphone:</p><br><p>$user_tel</p>
                        <p>Mot de passe:</p><br><p>$user_pwd</p>
                        <button id="btn-submit">Modifier Informations</button>
                    </div>
                    <div id="prof_stat">
                        <p>Nombre de favoris:</p><br><p>$nb_fav_user</p>
                        <p>Nombre de commentaires:</p><br><p>$nb_com_user</p>
                        <p>Date dernier commentaire:</p><br><p>$last_com_user</p>
                    </div>
                </section>
                <section id="prof_gal">
                     <div class="rowProjet">
                        <div class="column">
                            <a href="#img1"><img src="../img/redscale/redscale_2016_01.jpg"></a>
                            <a href="#img2"><img src="../img/redscale/redscale_2016_09.jpg"></a>
                            <a href="#img3"><img src="../img/redscale/redscale_2016_12.jpg"></a>
                        </div>
                        <div class="column">
                            <a href="#img4"><img src="../img/redscale/redscale_2016_03.jpg"></a>
                            <a href="#img5"><img src="../img/redscale/redscale_2016_13.jpg"></a>
                            <a href="#img6"><img src="../img/redscale/redscale_2016_04.jpg"></a>
                        </div>
                        <div class="column">
                            <a href="#img7"><img src="../img/redscale/redscale_2016_10.jpg"></a>
                            <a href="#img8"><img src="../img/redscale/redscale_2016_06.jpg"></a>
                            <a href="#img9"><img src="../img/redscale/redscale_2016_14.jpg"></a>
                        </div>
                    </div>
                </section>
                <!-- RESPONSIVE -->
                <section class="main-res">
                    <div id="imgBack-res">
                        <div id="idRes">
                            <h1>Léa<br>Buffet</h1>
                            <h2>GRAPHISTE</h2>
                        </div>
                    </div>
                    <div id="contact-res">
                        <div id="info">
                            <a href="https://goo.gl/maps/cDPoqEymfnS2">6 rue Friesé<br>67000 Strasbourg</a>
                            <p>+33(0)6.72.39.74.72<br>lbuffet5@gmail.com</p>
                        </div>
                        <form id="form-Res">
                            <p>CONTACTEZ MOI :</p>
                            <div class="form-group" style="background: none;">
                                <input type="nom" class="form-control" id="formNom" placeholder="nom, prénom">
                            </div>
                            <div class="form-group" style="background: none;">
                                <input type="email" class="form-control" id="formMail" placeholder="email">
                            </div>
                            <div class="form-group" style="background: none;">
                                <textarea class="form-control" id="FormText" rows="3" placeholder="votre message..."></textarea>
                            </div>
                            <div class="form-group">
                             <input id="btn-submit" type="submit" value="ENVOYER">
                        </div>
                        </form>
                    </div>
                </section>
                <!-- ---------- -->
            </main>
            <footer>
                <div class="bas">
                    <a href="../html/mentions.html">Mentions Légales</a>
                    <p><a href="../html/plan.html">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
            <!-- RESPONSIVE -->
            <div class="socialmedia-resp">
                <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
                <a id="instagram"href="https://www.instagram.com/_l.bft/">instagram</a>
            </div>
            <footer id="footer-responsive" style="margin-top: 350px;">
                
                <div class="bas">
                    <a href="../html/mentions.html">Mentions Légales</a>
                    <a href="../html/plan.html">Plan du site</a>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
            <!-- ---------- -->
            <script src="../JS/slideshow.js"></script>
            <script src="../JS/menu.js"></script>
        </body>
    </html>