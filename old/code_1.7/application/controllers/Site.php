<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
    public function __construct(){
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('session');
    }

    public function index() {
        $data["title"] = "Home"; //pas utile pour l'instant
        $data["currentPage"] = "home"; //voir pour class active nav bar

        $this->load->view('site/index'); //charge le views
    }
    
    public function about() {
    	$data["title"] = "About";

        $this->load->view('apropos'); //charge le views
    }
    
    public function gallery() {
    	$data["title"] = "Gallery";

        $this->load->view('gallery'); //charge le views
    }

    public function contact() {
    	$data["title"] = "Contact";

        $this->load->view('contact'); //charge le views
    }

    public function user() {
    	$data["title"] = "User";

        $this->load->view('user'); //charge le views
    }

	public function mentions() {
    	$data["title"] = "Mentions légales";

        $this->load->view('mentions'); //charge le views
    }

    public function plan() {
    	$data["title"] = "Plan du site";

        $this->load->view('plan'); //charge le views
    }

    //page de galeries
    public function redscale() {

        $this->load->view('redscale'); //charge le views
    }
    public function esad() {

        $this->load->view('esad'); //charge le views
    }
    public function loire() {

        $this->load->view('loire'); //charge le views
    }


    public function profil() {
        if($this->session->userdata('data')){
            $this->load->view('profil');
            
        } else {
            redirect("https://buflea-srv6.formations-web.alsace/index.php/Site/login");
        }
    }

    public function login() {
    	//$data["title"] = "Login";

        $this->load->view('loginForm'); //charge le views
    }
    
    public function signup() {
    	//$data["title"] = "Registration";
        $this->load->view('signup'); //charge le views
	}

    public function logout(){
        session_destroy();

        redirect("https://buflea-srv6.formations-web.alsace/index.php/");
    }


}