<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model('User_Model');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');

    }

    public function login() {
        $this->form_validation->set_rules('user_mail', 'Email', 'required|valid_email|is_unique[users.user_mail]');
        $this->form_validation->set_rules('user_pwd', 'Password', 'required');
        $this->form_validation->set_rules('confirm_user_pwd', 'Password Confirmation','required|matches[mot_de_passe]', array('matches' => 'Le mot de passe doit être identique.'));

        $data = $this->User_Model->login($_POST['user_mail'], $_POST['user_pwd']);
        if($data == false){
            redirect("https://buflea-srv6.formations-web.alsace/index.php/site/login");
        } else {
            $this->session->set_userdata('data', $data);
            redirect("https://buflea-srv6.formations-web.alsace/index.php/site/profil");
        }
        
    }

    public function logout(){
        session_destroy();

        redirect("https://buflea-srv6.formations-web.alsace/index.php/");
    }

    public function signup(){
        $data = $this->User_Model->signup(
            $_POST['user_first_name'],
            $_POST['user_last_name'],
            $_POST['user_mail'],
            $_POST['user_tel'],
            $_POST['user_pwd']
        );
    }

    public function modify(){
        if($_POST['user_pwd']){
            $this->form_validation->set_rules('user_pwd', 'Mot de passe');
            $this->form_validation->set_rules('user_pwd_conf', 'Confirmation de mot de passe', 'matches[user_pwd]',
            array('matches' => 'Les mots de passe ne correspondent pas !'));
            
            if ($this->form_validation->run() == FALSE) {
                print_r('<p>'.validation_errors().'</p>');
            } else {
                $newdata = $this->User_Model->modify($_POST['user_mail'], $_POST['user_tel'], $_POST['user_pwd'],$_SESSION['data']['0']['userID']);
                $this->session->set_userdata('data', $newdata);
                redirect("https://buflea-srv6.formations-web.alsace/index.php/profil");
            }
        } else {
            $newdata = $this->User_Model->modify($_POST['user_mail'], $_POST['user_tel'], $_POST['user_pwd'],$_SESSION['data']['0']['userID']);
            print_r($newdata);
            $this->session->set_userdata('data', $newdata);
            redirect("https://buflea-srv6.formations-web.alsace/index.php/profil");
        }


    }
    
}

