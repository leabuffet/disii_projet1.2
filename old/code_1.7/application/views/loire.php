<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>GALLERY - 35 LOIRE</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="shortcut icon" type="img/png" href="/assets/img/favicon_lbu.png" />
    </head>
    <body>
         <?php 
            include('header_gall.php');
        ?>
            <section>
                <div class="row">
                    <div class="column">
                        <a href="#img1"><img src="/assets/img/loire/LOIRE_PONT004.jpg"></a>
                        <a href="#img2"><img src="/assets/img/loire/LOIRE_PONT005.jpg"></a>
                        <a href="#img3"><img src="/assets/img/loire/LOIRE_PONT031.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img4"><img src="/assets/img/loire/LOIRE_PONT008.jpg"></a>
                        <a href="#img5"><img src="/assets/img/loire/LOIRE_PONT009.jpg"></a>
                        <a href="#img6"><img src="/assets/img/loire/LOIRE_PONT033.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img7"><img src="/assets/img/loire/LOIRE_PONT012.jpg"></a>
                        <a href="#img8"><img src="/assets/img/loire/LOIRE_PONT020.jpg"></a>
                    </div>
                    <div class="column">
                        <a href="#img9"><img src="/assets/img/loire/LOIRE_PONT029.jpg"></a>
                        <a href="#img10"><img src="/assets/img/loire/LOIRE_PONT030.jpg"></a>
                    </div>
                </div>
                <div id="img-lightbox">
                    <a href="#_" class="lightbox" id="img1"><img src="/assets/img/loire/LOIRE_PONT004.jpg"></a>
                    <a href="#_" class="lightbox" id="img2"><img src="/assets/img/loire/LOIRE_PONT005.jpg"></a>
                    <a href="#_" class="lightbox" id="img3"><img src="/assets/img/loire/LOIRE_PONT031.jpg"></a>
                    <a href="#_" class="lightbox" id="img4"><img src="/assets/img/loire/LOIRE_PONT008.jpg"></a>
                    <a href="#_" class="lightbox" id="img5"><img src="/assets/img/loire/LOIRE_PONT009.jpg"></a>
                    <a href="#_" class="lightbox" id="img6"><img src="/assets/img/loire/LOIRE_PONT033.jpg"></a>
                    <a href="#_" class="lightbox" id="img7"><img src="/assets/img/loire/LOIRE_PONT012.jpg"></a>
                    <a href="#_" class="lightbox" id="img8"><img src="/assets/img/loire/LOIRE_PONT020.jpg"></a>
                    <a href="#_" class="lightbox" id="img9"><img src="/assets/img/loire/LOIRE_PONT029.jpg"></a>
                    <a href="#_" class="lightbox" id="img10"><img src="/assets/img/loire/LOIRE_PONT030.jpg"></a>
                </div>
            </section>
             <section>
                <div class="cont-btn-back">
                    <a class="btn-back" href="/index.php/gallery" >other gallery</a>
                </div>
            </section>
        </main>
        <?php 
        include('footer.php');
        ?>
</html>