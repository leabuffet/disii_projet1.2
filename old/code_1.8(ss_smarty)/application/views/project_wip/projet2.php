<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>PROJECT - CAMELLIA SINENSIS</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../css/style.css">
        <link rel="shortcut icon" type="img/png" href="../img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="../index.html">HOME</a></li>
                    <li><a href="../html/apropos.html">ABOUT</a></li>
                    <li><a class="navActive" href="../html/project.html">PROJECT</a></li>
                    <li><a href="../html/gallery.html">GALLERY</a></li>
                    <li><a href="../html/contact.html">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <main id="sub">
            <!-- RESPONSIVE -->
            <section class="main-res" style ="height: 50px;">
                <div id="cont-btn-back">
                    <a id="btn-back" href="../html/gallery.html" >BACK</a>
                </div>
            </section>
            <!-- ---------- -->
            <div id="contentProjet">
                <section id="textProjet" class="gauche">
                    <div class="detailProjet">
                        <h2>Camellia Sinensis - identité visuelle (fictive)</h2>
                        <p>Charte graphique et identité visuelle pour une maison de thé fictive.
Camellia Sinensis est le nom de la plante de thé, aussi l’illustration du logo est un herbier de la feuille de thé. 
La couleur et le dessin vectoriel ont été ajoutés pour moderniser le logo tout en gardant l’impression de naturel affiliée à la structure.</p>
                    </div>
                </section>
                <section class="imgProjet">
                    <div class="rowProjet">
                        <div class="column">
                            <a href="#img1"><img src="../img/redscale/redscale_2016_01.jpg"></a>
                            <a href="#img2"><img src="../img/redscale/redscale_2016_09.jpg"></a>
                            <a href="#img3"><img src="../img/redscale/redscale_2016_12.jpg"></a>
                        </div>
                        <div class="column">
                            <a href="#img4"><img src="../img/redscale/redscale_2016_03.jpg"></a>
                            <a href="#img5"><img src="../img/redscale/redscale_2016_13.jpg"></a>
                            <a href="#img6"><img src="../img/redscale/redscale_2016_04.jpg"></a>
                        </div>
                        <div class="column">
                            <a href="#img7"><img src="../img/redscale/redscale_2016_10.jpg"></a>
                            <a href="#img8"><img src="../img/redscale/redscale_2016_06.jpg"></a>
                            <a href="#img9"><img src="../img/redscale/redscale_2016_14.jpg"></a>
                        </div>
                    </div>
                    <div id="img-lightbox">
                        <a href="#_" class="lightbox" id="img1"><img src="../img/redscale/redscale_2016_01.jpg"></a>
                        <a href="#_" class="lightbox" id="img2"><img src="../img/redscale/redscale_2016_09.jpg"></a>
                        <a href="#_" class="lightbox" id="img3"><img src="../img/redscale/redscale_2016_12.jpg"></a>
                        <a href="#_" class="lightbox" id="img4"><img src="../img/redscale/redscale_2016_03.jpg"></a>
                        <a href="#_" class="lightbox" id="img5"><img src="../img/redscale/redscale_2016_13.jpg"></a>
                        <a href="#_" class="lightbox" id="img6"><img src="../img/redscale/redscale_2016_04.jpg"></a>
                        <a href="#_" class="lightbox" id="img7"><img src="../img/redscale/redscale_2016_10.jpg"></a>
                        <a href="#_" class="lightbox" id="img8"><img src="../img/redscale/redscale_2016_06.jpg"></a>
                        <a href="#_" class="lightbox" id="img9"><img src="../img/redscale/redscale_2016_14.jpg"></a>
                        <a href="#_" class="lightbox" id="img10"><img src="../img/redscale/redscale_2016_11.jpg"></a>
                        <a href="#_" class="lightbox" id="img11"><img src="../img/redscale/redscale_2016_08.jpg"></a>
                        <a href="#_" class="lightbox" id="img12"><img src="../img/redscale/redscale_2016_15.jpg"></a>
                    </div>
                </section>
            </div>
            <section>
                <div class="cont-btn-back">
                    <a class="btn-back" href="project.html" >other project</a>
                </div>
            </section>
        </main>
        <footer>
            <div class="bas">
                <a href="../html/mentions.html">Mentions Légales</a>
                <p><a href="../html/plan.html">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- RESPONSIVE -->
        <footer id="footer-responsive">
            <div class="bas">
                <a href="../html/mentions.html">Mentions Légales</a>
                <a href="../html/plan.html">Plan du site</a>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- ---------- -->
        <script src="../JS/slideshow.js"></script>
    </body>
</html>