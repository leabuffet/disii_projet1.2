<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>HOME</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="shortcut icon" type="img/png" href="img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index">HOME</a></li>
                    <li><a href="/index.php/about">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a class="navActive" href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <main id="sub">
            <!-- RESPONSIVE -->
            <section class="main-res" style ="height: 50px;">
                <div id="cont-btn-back">
                    <a id="btn-back" href="/index.php/gallery">BACK</a>
                </div>
            </section>
            <!-- ---------- -->
            <div id="contentComGal">
                <section id="comSectGauche">
                    <div id="comSect">
                        <div>
                        <h2>$com_author - $com_date</h2>
                        <p>$com_content</p>
                        <br>
                        <h2>$com_author - $com_date</h2>
                        <p>$com_content</p>
                        <br>
                        <h2>$com_author - $com_date</h2>
                        <p>$com_content</p>
                        <br> 
                        </div>
                        <div>               
                       <form method="post" action="/index.php/user/add_comment" id="formCom">
                            <div class="form-group">
                               <input type="text" name="com_author" class="form-control" id="com_author" placeholder="pseudo">
                            </div>
                            <div class="form-group">
                               <textarea name="com_content" class="form-control" id="FormTextCom" rows="3" placeholder="votre commentaire..."></textarea>
                            </div>
                            <div class="form-group">
                                <input id="btn-submit" type="submit" value="COMMENTER">
                            </div>
                        </form>
                    </div>
                    </div>



                </section>
                <section id="imgComGal">
                    <a href="/index.php/esad"><img src="/assets/img/esad/esad10.jpg"></a>
                </section>
            </div>
            <section>
                <div class="cont-btn-back">
                    <a class="btn-back" href="/index.php/gallery" >other gallery</a>
                </div>
            </section>
        </main>
        <footer>
            <div class="bas">
                <a href="/index.php/mentions">Mentions Légales</a>
                <p><a href="/index.php/plan">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Loire, Esad</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- RESPONSIVE -->
        <div class="socialmedia-resp">
            <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
            <a id="instagram"href="https://www.instagram.com/_l.bft/">instagram</a>
        </div>
        <footer id="footer-responsive">
            <div class="bas">
                <a href="/index.php/mentions">Mentions Légales</a>
                <a href="/index.php/plan">Plan du site</a>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- ---------- -->
        <script src="/assets/JS/slideshow.js"></script>
        <script src="/assets/JS/menu.js"></script>
    </body>
</html>