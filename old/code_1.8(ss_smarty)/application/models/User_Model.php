<?php
    class User_Model extends CI_Model {

        public function __construct(){
            $this->load->database();
        }
        
        public function signup($user_first_name, $user_last_name, $user_mail, $user_tel, $user_pwd){
            $this->user_first_name  = $user_first_name;
            $this->user_last_name   = $user_last_name;
            $this->user_mail        = $user_mail;
            $this->user_tel         = $user_tel;
            $this->user_pwd         = password_hash($user_pwd, PASSWORD_BCRYPT);
            $this->user_status      = "User";

            $this->db->insert('users', $this);
        }

        public function login($user_mail, $user_pwd){
            $this->db->select('user_pwd');
            $this->db->from('users');
            $this->db->where('user_mail', $user_mail);
            $hash = $this->db->get();
            $row = $hash->row(); 

            if(isset($row)){
                if(password_verify($user_pwd, $row->user_pwd)) {
                    $this->db->select('userID, user_first_name, user_last_name, user_mail, user_tel, user_status');
                    $this->db->from('users');
                    $this->db->where('user_mail', $user_mail);
                    $data = $this->db->get();
                    return $data->result_array();
                }
            }

        }

        public function modify($user_mail, $user_tel, $user_pwd, $id){
            if($user_mail != ""){
                $this->user_mail = $user_mail;
            }

            if($user_tel != ""){
                $this->user_tel = $user_tel;
            }
     
            if($user_pwd != ""){
                $this->user_pwd = password_hash($user_pwd, PASSWORD_BCRYPT);
            }

            $this->db->update('users', $this, array('userID' => $id));

            $this->db->select('userID, user_mail, user_tel, user_first_name, user_last_name');
            $this->db->from('users');
            $this->db->where('userID', $id);

            $query = $this->db->get();
            return $query->result_array();

        }
   
        public function add_comment ($com_author, $com_content, $com_nom_gal){
            $this->com_content = $com_content;
            $this->com_author  = $com_author;
            $this->com_nom_gal = $com_nom_gal;

            $this->db->insert('comments', $this);
        }






    }

?>