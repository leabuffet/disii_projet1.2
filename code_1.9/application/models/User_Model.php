<?php
    /**
     * @file User_Model.php
     * @author Léa Buffet
     * @version 1.9
     * @date 22/05/2019
     * @brief Model pour gérer les données liées à l'utilisateur
     *
     * @details
     * <p>Ce modèle permet de récupérer l'ensemble des données des utilisateur</p>
     * <p>Les actions sont :</p>
     * <ul>
     *  <li><strong>signup</strong> : Création de nouvelles données "users" dans la base de données</li>
     *  <li><strong>login</strong> : Comparaison des données récupérées à celles de la base de données</li>
     *  <li><strong>modify</strong> : Mise à jour des informations de profil dans la base données</li>
     *  <li><strong>add_comment</strong> : Création de nouvelles données "comments" dans la base de données</li>
     * </ul>
     */

    class User_Model extends CI_Model {

        public function __construct(){
            $this->load->database();
        }
        
        /** récupère data du controller //
        * rempli les champs de la bdd avec données correspondante //
        * hashage du mdp //
        * requête sql pour insérer dans bdd
        */
        public function signup($user_first_name, $user_last_name, $user_mail, $user_tel, $user_pwd){
            //recup données du form et rempli champs bdd
            $this->user_first_name  = $user_first_name;
            $this->user_last_name   = $user_last_name;
            $this->user_mail        = $user_mail;
            $this->user_tel         = $user_tel;
            $this->user_pwd         = password_hash($user_pwd, PASSWORD_BCRYPT);
            $this->user_status      = "User";

            //requete vers bdd
            $this->db->insert('users', $this);
        }

        /** récupère data du controller //
        * compare mdp avec la bdd //
        * si identique renvoi les données de profil dans la session
        */
        public function login($user_mail, $user_pwd){
            //verifie cohérence mdp/login depuis bdd
            $this->db->select('user_pwd');
            $this->db->from('users');
            $this->db->where('user_mail', $user_mail);
            $hash = $this->db->get();
            $row = $hash->row(); 

            if(isset($row)){
                if(password_verify($user_pwd, $row->user_pwd)) {
                    $this->db->select('userID, user_first_name, user_last_name, user_mail, user_tel, user_status');
                    $this->db->from('users');
                    $this->db->where('user_mail', $user_mail);
                    $data = $this->db->get();
                    return $data->result_array();
                }
            }

        }

        /** récupère data du controller //
        * vérifie si formulaire contient des données //
        * màj de la bdd des champs correspondant //
        * hashage du mdp si modifié
        */
        public function modify($user_mail, $user_tel, $user_pwd, $id){
            //vérifie si le form contient des données pour les récup
            if($user_mail != ""){
                $this->user_mail = $user_mail;
            }

            if($user_tel != ""){
                $this->user_tel = $user_tel;
            }
     
            if($user_pwd != ""){
                $this->user_pwd = password_hash($user_pwd, PASSWORD_BCRYPT);
            }

            //màj de la bdd avec données récup
            $this->db->update('users', $this, array('userID' => $id));

            $this->db->select('userID, user_mail, user_tel, user_first_name, user_last_name');
            $this->db->from('users');
            $this->db->where('userID', $id);

            $query = $this->db->get();
            return $query->result_array();

        }
   
        /** récupère data du controller //
        * rempli les champs de la bdd avec données correspondante
        * requête sql pour insérer dans bdd
        */
        public function add_comment ($com_author, $com_content, $com_nom_gal){
            //défini les données du form pour incorporer dans la bdd
            $this->com_content = $com_content;
            $this->com_author  = $com_author;
            $this->com_nom_gal = $com_nom_gal;

            //requete pour remplir bdd avec données recup
            $this->db->insert('comments', $this);
        }






    }

?>