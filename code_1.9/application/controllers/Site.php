<?php
    /**
     * @file Site.php
     * @author Léa Buffet
     * @version 1.9
     * @date 22/05/2019
     * @brief Controller pour la gestion des pages
     *
     * @details
     * <p>Cette classe permet de gérer l'affichage des pages statiques</p>
     * <p>Les actions sont :</p>
     * <ul>
     *  <li><strong>parse</strong> : Affichage des pages statiques</li>
     * </ul>
     */

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
    public function __construct(){
        parent::__construct();

        //charge helper codeigniter (simplifie url)
        $this->load->helper('url');
        //charge librairie codeigniter pour session
        $this->load->library('session');
        //charge smarty
        $this->load->library('parser');
    }

    public function index() {
        $data["title"] = "Home"; //données stockées pour affichage smarty
        $data["currentPage"] = "home"; //voir pour class active sur nav bar

        $this->load->view('site/index'); //charge le views
    }


    public function about() {
    	$data["title"] = "About";

        //$this->load->view('apropos'); //charge le view
        $this->parser->parse("apropos", $data); //charge le view.tpl (smarty)
    }
    
    public function gallery() {
    	$data["title"] = "Gallery";

        //$this->load->view('gallery'); 
        $this->parser->parse("gallery", $data);
        $this->parser->parse("templates/footer");
    }

    public function contact() {
    	$data["title"] = "Contact";

        //$this->load->view('contact'); 
        $this->parser->parse("contact", $data);
    }


	public function mentions() {
    	$data["title"] = "Mentions légales";

        //$this->load->view('mentions'); 
        $this->parser->parse("mentions", $data);
    }

    public function plan() {
    	$data["title"] = "Plan du site";

        //$this->load->view('plan'); 
        $this->parser->parse("plan", $data);
    }

    //page de galeries
    public function redscale() {

        //$this->load->view('redscale'); 
        $this->parser->parse("templates/header_gall");
        $this->parser->parse("galleries/redscale");
        $this->parser->parse("templates/footer");
    }
    public function esad() {

        //$this->load->view('esad'); 
        $this->parser->parse("templates/header_gall");
        $this->parser->parse("galleries/esad");
        $this->parser->parse("templates/footer");
    }
    public function loire() {

        //$this->load->view('loire'); 
        $this->parser->parse("templates/header_gall");
        $this->parser->parse("galleries/loire");
        $this->parser->parse("templates/footer");
    }

    //commentaire de galerie
    public function com_redscale() {
        //vérifie si qqn connecté
        if($this->session->userdata('data')){
            //si connexion charge page de commentaire affilié
            //$this->load->view('com_redscale');
            $this->parser->parse("galleries/com_redscale");
        }else{
            //si pas de connexion renvoie vers page login
            redirect("/login");
        }
    }
    public function com_loire() {
        if($this->session->userdata('data')){
            //$this->load->view('com_loire');
            $this->parser->parse("galleries/com_loire");
        }else{
            redirect("/login");
        }      
    }
    public function com_esad() {
        if($this->session->userdata('data')){
            //$this->load->view('com_esad');
            $this->parser->parse("galleries/com_esad");
        }else{
            redirect("/login");
        } 
    }

    public function profil() {
        $data['session'] = $_SESSION['data'][0];

        //vérifie si données de connexion, redirige vers profil
        if($this->session->userdata('data')){
            //$this->load->view('profil');
            $this->parser->parse("profil", $data);
        //sinon redirige vers login
        } else {
            redirect("/login");
        }
    }

    public function login() {
    	//$this->load->view('loginForm');
        $this->parser->parse("loginForm");
    }
    
    public function signup() {
        //$this->load->view('signup');
        $this->parser->parse("signup");
	}

    public function logout(){
        //détruit la session pour se déconnecter
        session_destroy();

        redirect("/index");
    }

    public function aide() {
        $this->parser->parse("aide");
    }
}