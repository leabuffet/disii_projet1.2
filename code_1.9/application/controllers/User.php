<?php
    /**
     * @file User.php
     * @author Léa Buffet
     * @version 1.9
     * @date 22/05/2019
     * @brief Controller pour la gestion fonctions liées à l'utilisateur
     *
     * @details
     * <p>Cette classe permet de gérer l'ensemble des fonctionnalités de l'utilisateur</p>
     * <p>Les actions sont :</p>
     * <ul>
     *  <li><strong>login</strong> : Connexion au profil</li>
     *  <li><strong>logout</strong> : Déconnexion du profil</li>
     *  <li><strong>signup</strong> : Inscription</li>
     *  <li><strong>modify</strong> : Modification des informations personnelles</li>
     *  <li><strong>add_comment</strong> : Ajout de commentaire</li>
     * </ul>
     */


defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model('User_Model');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');

    }

    /** récupère info du formulaire et compare à la bdd //
     * si données fausse renvoi vers login, sinon renvoi vers profil
     */
    public function login() {
        $this->form_validation->set_rules('user_mail', 'Email', 'required|valid_email|is_unique[users.user_mail]');
        $this->form_validation->set_rules('user_pwd', 'Password', 'required');
        $this->form_validation->set_rules('confirm_user_pwd', 'Password Confirmation','required|matches[mot_de_passe]', array('matches' => 'Le mot de passe doit être identique.'));

        $data = $this->User_Model->login($_POST['user_mail'], $_POST['user_pwd']);

        if($data == false){
            redirect("/login");
        } else {
            $this->session->set_userdata('data', $data);
            redirect("/profil");
        }
        
    }

    /** détruit session pour déconnecter le user
     */
    public function logout(){
        session_destroy();

        redirect("");
    }

    /** récupère info du formulaire //
     * renvoi les infos vers user_model pour ajouter à la bdd
     */
    public function signup(){
        $data = $this->User_Model->signup(
            $_POST['user_first_name'],
            $_POST['user_last_name'],
            $_POST['user_mail'],
            $_POST['user_tel'],
            $_POST['user_pwd']
        );
        redirect("/profil");
    }

    /** récupère info du formulaire //
     * vérifie si mdp et confirmation mdp sont identique, sinon redirige vers profil //
     * si pas d'erreur renvoi vers user_model pour màj de la bdd
     */
    public function modify(){
        if($_POST['user_pwd']){
            $this->form_validation->set_rules('user_pwd', 'Mot de passe');
            $this->form_validation->set_rules('user_pwd_conf', 'Confirmation de mot de passe', 'matches[user_pwd]',
            array('matches' => 'Les mots de passe ne correspondent pas !'));
            
            if ($this->form_validation->run() == FALSE) {
                print_r('<p>'.validation_errors().'</p>');
            } else {
                $newdata = $this->User_Model->modify($_POST['user_mail'], $_POST['user_tel'], $_POST['user_pwd'],$_SESSION['data']['0']['userID']);
                $this->session->set_userdata('data', $newdata);
                redirect("/profil");
            }
        } else {
            $newdata = $this->User_Model->modify($_POST['user_mail'], $_POST['user_tel'], $_POST['user_pwd'],$_SESSION['data']['0']['userID']);
            print_r($newdata);
            $this->session->set_userdata('data', $newdata);
            redirect("/profil");
        }
    }
    

    /** récupère info du formulaire //
     * envoi les infos vers user_model
     */    
    public function add_comment(){
        $data = $this->User_Model->add_comment(
            $_POST['com_author'],
            $_POST['com_content'],
            $_POST['com_nom_gal']
        );

        redirect("/gallery");

       
    }
}

