<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>PLAN DU SITE</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="shortcut icon" type="img/png" href="/assets/img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
               <ul>
                    <li><a class="navActive" href="index">HOME</a></li>
                    <li><a href="/index.php/">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="/assets/img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="/assets/img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a class="navActive" href="index">HOME</a></li>
                    <li><a href="/index.php/">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section id="plan">
<h2>Plan du site</h2>
<h3>Accueil :</h3>
<p>Galerie d'images</p>
<h3>About :</h3>
<p>Présentation<br>Lien vers réseaux sociaux</p>
<h3>Project :</h3>
<p><strong>Miniatures des projets:</strong>
    <br>01 - 40&+ >> Galerie d'image
    <br>02 - Camellia Sinensis >> Texte de présentation / Galerie d'image
    <br>03 - Déformation >> Texte de présentation / Galerie d'image
    <br>04 - Abandonnée >> Texte de présentation / Galerie d'image
    <br>05 - Perdu >> Texte de présentation / Galerie d'image
    <br>06 - Portraits >> Texte de présentation / Galerie d'image
</p>
<h3>Gallery :</h3>
<p><strong>Miniatures des galeries :</strong>
    <br>110 - Redscale 2016 - 2018 / Lien vers commentaires >> Galerie d'images / Formulaire d'envoi de commentaires
    <br>35 - Loire 2016 / Lien vers commentaires >> Galerie d'images / Formulaire d'envoi de commentaires
    <br>35 - Esad 2014-2015 / Lien vers commentaires >> Galerie d'images / Formulaire d'envoi de commentaires
</p>
<h3>Contact :</h3>
<p>Formulaire de contact<br>Informations de contact<br>Liens vers les réseaux sociaux</p>
<h3>User :</h3>
<p><strong>Login</strong><br>Formulaire de connexion<br>Lien vers inscription >> Formulaire d'inscription <br>Lien vers Aide <br>
    <strong>Profil</strong><br>Affichage des données de profil<br>Formulaire de modification des informations de profil<br>Lien de déconnexion    
</p>
</section>
        </main>
        <footer>
            <div class="bas">
                <a href="/index.php/mentions">Mentions Légales</a>
                <p><a href="/index.php/plan">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- RESPONSIVE -->
            <footer id="footer-responsive">
                
                <div class="bas">
                    <a href="/index.php/mentions">Mentions Légales</a>
                    <a href="/index.php/plan">Plan du site</a>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
        <!-- ---------- -->
        <script src="/assets/JS/slideshow.js"></script>
         <script src="/assets/JS/menu.js"></script>
    </body>
</html>