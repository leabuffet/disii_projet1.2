<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>AIDE EN LIGNE</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="shortcut icon" type="img/png" href="/assets/img/favicon_lbu.png" />
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a class="navActive" href="index">HOME</a></li>
                    <li><a href="/index.php/about">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="/assets/img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="/assets/img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a class="navActive" href="index">HOME</a></li>
                    <li><a href="/index.php/">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section id="mentions">
            <h2>Aide en ligne</h2>
<h3>Utilisation:</h3>
<div id="liste">
    <li>Pour consulter la présentation du graphiste, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/about"> ce lien</a>.</li>
    <li>Pour parcourir les différents projets, vous pouvez suivre <a style="color: #3eb37f;" href="#">ce lien</a>.</li>
    <li>Pour parcourir les galeries d'images, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/about">ce lien</a>.</li>
    <li>Pour contacter le graphiste, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/contact">ce lien</a>.</li>
    <li>Pour accéder à l'espace utilisateur, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/login">ce lien</a>.</li>
    <li>Pour parcourir les galeries d'images, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/about">ce lien</a>.</li>
    <li>Pour revenir à la page d'accueil, vous pouvez suivre <a style="color: #3eb37f;" href="/index.php/index">ce lien</a>.</li>
</div>
<h3>Connexion / Inscription:</h3>
<p>Pour accéder à toutes les fonctionnalités de ce site, vous devez posséder un compte et vous y connecter. <br>
Si vous ne posséder pas de compte, dirigez vous vers l’onglet <a style="color: #3eb37f;" href="/index.php/profil">USER</a> dans la barre de navigation situé en haut du site. <br>
Une fois sur la page du login, cliquez sur <a href="/index.php/signup" style="color: #3eb37f;">INSCRIPTION</a>, remplissez le formulaire qui s’affiche et votre compte sera créé. <br>
Une fois inscrit vous serez rediriger vers la page <a style="color: #3eb37f;" href="/index.php/login">LOGIN</a>. Remplissez le formulaire avec vos identifiant pour avoir accès à votre profil. <br>
Pour vous déconnecter, il vous suffit de cliquer sur le bouton logout sur votre page de profil.</p>

<h3>Modification des informations personnelles :</h3>
<p>Lorsque que vous êtes connecté vous pouvez consulter vos informations personnelles depuis votre profil. Pour accéder à votre profil, cliquer sur l’onglet <a style="color: #3eb37f;" href="/index.php/profil">USER</a> dans la barre de navigation principale. <br>
Pour modifier vos information personnelles, remplissez le formulaire attitré, pour le valider cliquez sur le bouton « modifier ». Lorsque vous appuyé sur le bouton les informations de gauche sont mise à jour automatiquement.</p>

<h3>Ajout de commentaires :</h3>
<p>Pour voir et commenter les galeries vous devez être connecté. <br>
Pour accéder aux commentaire, rendez-vous sur la page <a style="color: #3eb37f;" href="/index.php/gallery">GALLERY</a> en cliquant sur l’onglet affilié dans la barre de navigation principale, puis cliquez sur l’icone en forme de phylactère en fonction de la galerie que vous souhaitez consulter. <br>
Sur cette page de commentaire vous pourrez voir les derniers commentaires postés concernant la galerie sélectionnée. De plus, en remplissant les champs du formulaire en bas de page vous pouvez ajouter le votre. Vous devrez préciser le pseudo que vous souhaitez utiliser ainsi que choisir dans la liste déroulante, la galerie pour laquelle le commentaire est destinée.</p>
            </section>
        </main>
        <footer>
            <div class="bas">
                <a href="/index.php/mentions">Mentions Légales</a>
                <p><a href="/index.php/plan">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Canon, Minolta</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- RESPONSIVE -->
            <footer id="footer-responsive">
                
                <div class="bas">
                    <a href="/index.php/mentions">Mentions Légales</a>
                    <a href="/index.php/plan">Plan du site</a>
                    <p>© Léa Buffet - 2018</p>
                </div>
            </footer>
        <!-- ---------- -->
        <script src="/assets/JS/slideshow.js"></script>
         <script src="/assets/JS/menu.js"></script>
    </body>
</html>