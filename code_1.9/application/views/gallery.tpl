<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>GALLERY</title>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/assets/css/style.css">
        <link rel="shortcut icon" type="img/png" href="/assets/img/favicon_lbu.png" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li><a href="index">HOME</a></li>
                    <li><a href="/index.php/about">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a class="navActive" href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- RESPONSIVE -->
        <div id="header-menu">
            <img id="logo" src="../img/LogoLB_wh.png" alt="logo">
            <span onclick="openMenu()"><img id="menu-btn"  src="/assets/img/menu_btn.png" alt=""></span>
        </div>
        <header id="header-responsive" class="toggle-menu">
            <nav>
                <ul>
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="/index.php/about">ABOUT</a></li>
                    <li><a href="#">PROJECT</a></li>
                    <li><a class="navActive" href="/index.php/gallery">GALLERY</a></li>
                    <li><a href="/index.php/contact">CONTACT</a></li>
                    <li><a href="/index.php/profil">USER</a></li>
                </ul>
            </nav>
        </header>
        <!-- --------- -->
        <main>
            <section id="gall-res">
                <div class="gallery">
                    <div>
                        <a href="/index.php/com_redscale" class="comLnk"><i class="far fa-comment-alt"></i></a>
                        <a class ="titre-gall" href="/index.php/redscale">110 - REDSCALE 2016 - 2018</a>
                    </div>
                    <a href="/index.php/redscale"><img src="/assets/img/redscale_cover.jpg"/></a>
                </div>
                <div class="gallery">
                    <div>
                        <a href="/index.php/com_loire" class="comLnk"><i class="far fa-comment-alt"></i></a>
                        <a class ="titre-gall" href="/index.php/loire">35 - LOIRE 2016 </a>
                    </div>
                    <a href="/index.php/loire"><img src="/assets/img/canon_cover.jpg"></a>
                </div>
                <div class="gallery">
                    <div>
                        <a href="/index.php/com_esad" class="comLnk"><i class="far fa-comment-alt"></i></a>
                        <a class ="titre-gall" href="/index.php/esad">35 - ESAD 2014 - 2015</a>
                    </div>
                    <a href="/index.php/esad"><img src="/assets/img/minolta_cover.jpg"></a>
                </div>
            </section>
        </main>
    <?php 
    include('footer.php');
     ?>
</html>