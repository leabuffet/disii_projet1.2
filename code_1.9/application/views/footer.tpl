<footer>
            <div class="bas">
                <a href="html/mentions.html">Mentions Légales</a>
                <p><a href="html/plan.html">Plan du site : </a>Accueil > À Propos // Portfolio // Galerie // Contact<br>À Propos > Projets: 01, 02, 03, 04, 05, 06<br>Galerie > Séries: Redscale, Loire, Esad</p>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- RESPONSIVE -->
        <div class="socialmedia-resp">
            <a id="twitter" href="https://twitter.com/l_buft">twitter</a>
            <a id="instagram"href="https://www.instagram.com/_l.bft/">instagram</a>
        </div>
        <footer id="footer-responsive">
            <div class="bas">
                <a href="views/mentions.php">Mentions Légales</a>
                <a href="views/plan.php">Plan du site</a>
                <p>© Léa Buffet - 2018</p>
            </div>
        </footer>
        <!-- ---------- -->
        <script src="JS/slideshow.js"></script>
        <script src="JS/menu.js"></script>
    </body>