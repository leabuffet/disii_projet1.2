-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 23 Avril 2019 à 09:08
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `f_buflea`
--
CREATE DATABASE IF NOT EXISTS `f_buflea` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `f_buflea`;

-- --------------------------------------------------------

--
-- Structure de la table `Album`
--

CREATE TABLE `Album` (
  `n_album` int(11) NOT NULL,
  `nom_album` varchar(50) NOT NULL,
  `label` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Album`
--

INSERT INTO `Album` (`n_album`, `nom_album`, `label`) VALUES
(1, 'En passant', 'nlab'),
(2, 'Chansons pour les pieds', 'nlab'),
(3, 'Live 96', 'nlab'),
(4, 'En chantant', 'nlab'),
(5, 'Le tour de France', 'nlab');

-- --------------------------------------------------------

--
-- Structure de la table `Concert`
--

CREATE TABLE `Concert` (
  `n_interprete` int(11) NOT NULL,
  `annee_concert` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Concert`
--

INSERT INTO `Concert` (`n_interprete`, `annee_concert`) VALUES
(1, 1986),
(1, 1990),
(1, 1994),
(1, 1998),
(1, 2002),
(2, 1993),
(2, 1997),
(3, 1988),
(3, 1993),
(4, 1991),
(4, 1997),
(5, 1997);

-- --------------------------------------------------------

--
-- Structure de la table `Interprete`
--

CREATE TABLE `Interprete` (
  `n_interprete` int(11) NOT NULL,
  `nom_interprete` varchar(50) NOT NULL,
  `prenom_interprete` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Interprete`
--

INSERT INTO `Interprete` (`n_interprete`, `nom_interprete`, `prenom_interprete`) VALUES
(1, 'Goldman', 'Jean_Jacques'),
(2, 'Sardou', 'Michel'),
(3, 'Gall', 'France'),
(4, 'Bruel', 'Patrick'),
(5, 'Trenet', 'Charles'),
(6, 'TEKIN', 'Anissa');

-- --------------------------------------------------------

--
-- Structure de la table `chanson`
--

CREATE TABLE `chanson` (
  `n_chanson` int(11) NOT NULL,
  `nom_chanson` varchar(100) NOT NULL,
  `annee_de_creation` int(11) NOT NULL,
  `n_interprete` int(11) NOT NULL,
  `n_album` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `chanson`
--

INSERT INTO `chanson` (`n_chanson`, `nom_chanson`, `annee_de_creation`, `n_interprete`, `n_album`) VALUES
(1, 'En passant', 1997, 1, 1),
(2, 'C\'est pas vrai', 2001, 1, NULL),
(3, 'Le Cor', 1994, 5, NULL),
(4, 'Comme d\'habitude', 1972, 2, NULL),
(5, 'Le France', 1976, 2, NULL),
(6, 'Ella, Elle l\'a', 1986, 3, 5),
(7, 'Bouge', 1994, 4, 3),
(8, 'Casser la voix', 1991, 4, 3),
(9, 'Pas toi', 1985, 1, NULL),
(10, 'Je vole', 1979, 2, 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Album`
--
ALTER TABLE `Album`
  ADD PRIMARY KEY (`n_album`);

--
-- Index pour la table `Interprete`
--
ALTER TABLE `Interprete`
  ADD PRIMARY KEY (`n_interprete`);

--
-- Index pour la table `chanson`
--
ALTER TABLE `chanson`
  ADD PRIMARY KEY (`n_chanson`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Album`
--
ALTER TABLE `Album`
  MODIFY `n_album` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `Interprete`
--
ALTER TABLE `Interprete`
  MODIFY `n_interprete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `chanson`
--
ALTER TABLE `chanson`
  MODIFY `n_chanson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;--
-- Base de données :  `f_buflea_`
--
CREATE DATABASE IF NOT EXISTS `f_buflea_` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `f_buflea_`;
--
-- Base de données :  `f_buflea_2311`
--
CREATE DATABASE IF NOT EXISTS `f_buflea_2311` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `f_buflea_2311`;

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `num_contact` int(11) NOT NULL,
  `nom_contact` varchar(30) NOT NULL,
  `prenom_contact` varchar(30) NOT NULL,
  `tel_contact` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`num_contact`, `nom_contact`, `prenom_contact`, `tel_contact`) VALUES
(1, 'Benallou', 'Nour', '0388967027'),
(2, 'Buffet', 'Léa', '0388163065'),
(3, 'Burckert', 'Léa', '0388768850'),
(4, 'Tekin', 'Anissa', '0388769708'),
(5, 'Vuilliet', 'Thomas', ''),
(6, 'Badea', 'Lucia', '0388375041'),
(7, 'Barkane', 'Myriam', '0388884759'),
(8, 'Beltzung', 'Steven', '0388862936'),
(9, 'Bihry', 'Alban', '0388872696'),
(10, 'Frey', 'Nicolas', '0388921518'),
(11, 'Oberlin-Martins', 'Arthur', '0388107472'),
(12, 'Pfister', 'Adrien', '0388119857'),
(13, 'Schneider', 'Jérémy', '0388824824'),
(14, 'Sénéchal', 'Julien', '0388520021'),
(15, 'Spahn', 'Antoine', '0388112909'),
(16, 'Stahl', 'David', '0388418705');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `num_entreprise` int(11) NOT NULL,
  `nom_entreprise` varchar(40) NOT NULL,
  `num_contact` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`num_entreprise`, `nom_entreprise`, `num_contact`) VALUES
(1, 'ATIWEB', 5),
(2, 'K2M SERVICES', 4),
(3, 'DIVALTO', 3),
(4, 'SNCF TECHNICENTRE', 2),
(5, 'ECOGREENENERGY', 1),
(6, 'TRIBE IT PARTNERS', 16),
(7, 'GEFCO INDUSTRIAL SERVICES', 15),
(8, 'UBICENTREX', 14),
(9, 'GEPIX', 13),
(10, 'TKDES', 12),
(11, 'CACTUS GRAPHISME', 11),
(12, 'PIXEL PLURIMEDIA', 10),
(13, 'NEOONE', 9),
(14, 'EIGHTY DESIGN', 8),
(15, 'HDR COMMUNICATIONS', 7),
(16, 'FM LOGISTIC CORPORATE', 6);

-- --------------------------------------------------------

--
-- Structure de la table `enveloppe`
--

CREATE TABLE `enveloppe` (
  `num_entreprise` int(11) NOT NULL,
  `num_taxe` int(11) NOT NULL,
  `annee` int(11) NOT NULL,
  `montant` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `enveloppe`
--

INSERT INTO `enveloppe` (`num_entreprise`, `num_taxe`, `annee`, `montant`) VALUES
(1, 2, 2015, 2000),
(1, 2, 2016, 2500),
(1, 2, 2017, 2200),
(1, 2, 2018, 2900),
(2, 1, 2018, 3200),
(3, 1, 2017, 1200),
(3, 1, 2018, 1800),
(4, 1, 2017, 1900),
(4, 2, 2017, 600),
(4, 1, 2018, 1900),
(4, 2, 2018, 900),
(5, 1, 2018, 3600),
(6, 1, 2016, 400),
(6, 1, 2017, 600),
(6, 1, 2018, 1200),
(7, 1, 2017, 500),
(7, 2, 2017, 400),
(7, 1, 2018, 900),
(7, 2, 2018, 1300),
(8, 1, 2018, 250),
(9, 2, 2016, 450),
(9, 2, 2017, 600),
(9, 2, 2018, 800),
(10, 2, 2015, 1500),
(10, 2, 2016, 1600),
(10, 2, 2017, 1900),
(10, 2, 2018, 2200),
(11, 2, 2017, 1500),
(11, 2, 2018, 1900),
(12, 1, 2018, 2000),
(12, 2, 2018, 400),
(13, 1, 2016, 400),
(13, 1, 2017, 500),
(13, 1, 2018, 800),
(14, 2, 2018, 700),
(15, 1, 2016, 1250),
(15, 1, 2017, 1400),
(15, 1, 2018, 1600),
(16, 1, 2017, 1100),
(16, 1, 2018, 1500);

-- --------------------------------------------------------

--
-- Structure de la table `secteur`
--

CREATE TABLE `secteur` (
  `num_secteur` int(11) NOT NULL,
  `nom_secteur` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `secteur`
--

INSERT INTO `secteur` (`num_secteur`, `nom_secteur`) VALUES
(1, 'Agroalimentaire'),
(2, 'Informatique / Télécoms'),
(3, 'Études et conseils'),
(4, 'Métallurgie / Travail du métal'),
(5, 'Services aux entreprises'),
(6, 'Transports / Logistique'),
(7, 'Plastique / Caoutchouc'),
(8, 'Électronique / Électricité'),
(9, 'Banque / Assurance'),
(10, 'Textile / Habillement / Chaussure');

-- --------------------------------------------------------

--
-- Structure de la table `taxe`
--

CREATE TABLE `taxe` (
  `num_taxe` int(11) NOT NULL,
  `nom_taxe` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `taxe`
--

INSERT INTO `taxe` (`num_taxe`, `nom_taxe`) VALUES
(1, 'Formation continue'),
(2, 'Apprentissage');

-- --------------------------------------------------------

--
-- Structure de la table `typologie`
--

CREATE TABLE `typologie` (
  `num_entreprise` int(11) NOT NULL,
  `num_secteur` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `typologie`
--

INSERT INTO `typologie` (`num_entreprise`, `num_secteur`) VALUES
(1, 5),
(1, 7),
(2, 8),
(3, 4),
(3, 9),
(3, 10),
(4, 1),
(4, 5),
(4, 6),
(5, 8),
(5, 2),
(6, 1),
(7, 6),
(7, 5),
(7, 9),
(8, 10),
(8, 1),
(8, 4),
(9, 2),
(10, 10),
(10, 9),
(10, 7),
(11, 4),
(11, 8),
(12, 10),
(12, 7),
(12, 9),
(13, 4),
(13, 5),
(13, 9),
(14, 1),
(14, 8),
(15, 2),
(16, 10),
(16, 4),
(16, 9);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`num_contact`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`num_entreprise`);

--
-- Index pour la table `secteur`
--
ALTER TABLE `secteur`
  ADD PRIMARY KEY (`num_secteur`);

--
-- Index pour la table `taxe`
--
ALTER TABLE `taxe`
  ADD PRIMARY KEY (`num_taxe`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `num_contact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `num_entreprise` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `secteur`
--
ALTER TABLE `secteur`
  MODIFY `num_secteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `taxe`
--
ALTER TABLE `taxe`
  MODIFY `num_taxe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;--
-- Base de données :  `f_buflea_Portfolio`
--
CREATE DATABASE IF NOT EXISTS `f_buflea_Portfolio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `f_buflea_Portfolio`;

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `comID` int(10) NOT NULL,
  `com_content` text NOT NULL,
  `com_date` date NOT NULL,
  `com_author` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`comID`, `com_content`, `com_date`, `com_author`) VALUES
(1, 'Lorem ipsum dolores sit', '2019-04-17', 'Léa'),
(2, 'consectetur adipiscing elit', '2019-04-18', 'John'),
(3, 'Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.', '2019-04-18', 'Jean');

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE `favoris` (
  `favID` int(10) NOT NULL,
  `fav_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `favoris`
--

INSERT INTO `favoris` (`favID`, `fav_date`) VALUES
(1, '2019-04-02'),
(2, '2019-04-04'),
(3, '2019-04-15'),
(4, '2019-04-15');

-- --------------------------------------------------------

--
-- Structure de la table `galleries`
--

CREATE TABLE `galleries` (
  `galID` int(10) NOT NULL,
  `gal_name` text NOT NULL,
  `gal_date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `galleries`
--

INSERT INTO `galleries` (`galID`, `gal_name`, `gal_date`) VALUES
(1, 'Bords de Loire', '07/2016'),
(2, 'Serre de Vienne', '04/2018');

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `imgID` int(10) NOT NULL,
  `img_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `images`
--

INSERT INTO `images` (`imgID`, `img_name`) VALUES
(1, 'BdL_01'),
(2, 'BdL_02'),
(3, 'BdL_03'),
(4, 'SdV_07'),
(5, 'SdV_09'),
(6, 'BdL_12');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `tagID` int(10) NOT NULL,
  `tag_name` text NOT NULL,
  `tag_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`tagID`, `tag_name`, `tag_description`) VALUES
(1, 'redscale', 'type de pellicule'),
(2, '110', 'format de pellicule'),
(3, '35', 'format de pellicule'),
(4, 'couleurs', 'type de pellicule'),
(5, 'noir_et_blanc', 'type de pellicule'),
(6, 'vegetal', 'sujet principal'),
(7, 'paysage', 'sujet principal'),
(8, 'animal', 'sujet principal');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `userID` int(10) NOT NULL,
  `user_mail` text NOT NULL,
  `user_tel` text NOT NULL,
  `user_pwd` text NOT NULL,
  `user_first_name` text NOT NULL,
  `user_last_name` text NOT NULL,
  `user_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`userID`, `user_mail`, `user_tel`, `user_pwd`, `user_first_name`, `user_last_name`, `user_status`) VALUES
(2, 'moderateur@mail.com', '01000000', 'passW0rd*', 'John', 'Doe', 'Modo'),
(1, 'lbuffet5@gmail.com', '0672397472', 'rlyeHfthagN', 'Léa', 'Buffet', 'Admin'),
(3, 'membre@mail.com', '02000000', 'Pa5Sw0rD', 'Jean', 'Neige', 'User');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
