-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 16 Mai 2019 à 12:03
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `f_buflea_Portfolio`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `comID` int(11) NOT NULL,
  `com_content` text NOT NULL,
  `com_date` date NOT NULL,
  `com_author` text NOT NULL,
  `userID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`comID`, `com_content`, `com_date`, `com_author`, `userID`) VALUES
(1, 'Lorem ipsum dolores sit', '2019-04-17', 'John', 1),
(2, 'consectetur adipiscing elit', '2019-04-18', 'Léa', 2),
(3, 'Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.', '2019-04-18', 'Jean', 3);

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE `favoris` (
  `favID` int(11) NOT NULL,
  `fav_date` date NOT NULL,
  `userID` int(10) NOT NULL,
  `imgID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `favoris`
--

INSERT INTO `favoris` (`favID`, `fav_date`, `userID`, `imgID`) VALUES
(1, '2019-04-02', 2, 2),
(2, '2019-04-04', 1, 2),
(3, '2019-04-15', 3, 1),
(4, '2019-04-15', 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `galleries`
--

CREATE TABLE `galleries` (
  `galID` int(11) NOT NULL,
  `gal_name` text NOT NULL,
  `gal_date` text NOT NULL,
  `imgID` int(10) NOT NULL,
  `tagID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `galleries`
--

INSERT INTO `galleries` (`galID`, `gal_name`, `gal_date`, `imgID`, `tagID`) VALUES
(1, 'Bords de Loire', '07/2016', 2, 6),
(2, 'Serre de Vienne', '04/2018', 4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `imgID` int(11) NOT NULL,
  `img_name` text NOT NULL,
  `galID` int(10) NOT NULL,
  `tagID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `images`
--

INSERT INTO `images` (`imgID`, `img_name`, `galID`, `tagID`) VALUES
(1, 'BdL_01', 1, 6),
(2, 'BdL_02', 1, 6),
(3, 'BdL_03', 1, 6),
(4, 'SdV_07', 2, 6),
(5, 'SdV_09', 2, 6),
(6, 'BdL_12', 1, 6);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `tagID` int(11) NOT NULL,
  `tag_name` text NOT NULL,
  `tag_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tags`
--

INSERT INTO `tags` (`tagID`, `tag_name`, `tag_description`) VALUES
(1, 'redscale', 'type de pellicule'),
(2, '110', 'format de pellicule'),
(3, '35', 'format de pellicule'),
(4, 'couleurs', 'type de pellicule'),
(5, 'noir_et_blanc', 'type de pellicule'),
(6, 'vegetal', 'sujet principal'),
(7, 'paysage', 'sujet principal'),
(8, 'animal', 'sujet principal');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `user_mail` text NOT NULL,
  `user_tel` text NOT NULL,
  `user_pwd` text NOT NULL,
  `user_first_name` text NOT NULL,
  `user_last_name` text NOT NULL,
  `user_status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`userID`, `user_mail`, `user_tel`, `user_pwd`, `user_first_name`, `user_last_name`, `user_status`) VALUES
(1, 'moderateur@mail.com', '01000000', 'passW0rd*', 'John', 'Doe', 'Modo'),
(2, 'lbuffet5@gmail.com', '0672397472', '12345678', 'Léa', 'Buffet', 'Admin'),
(3, 'membre@mail.com', '02000000', 'Pa5Sw0rD', 'Jean', 'Neige', 'User'),
(4, '1212121212', '1234', '$2y$10$IjEm5w1xfga1LlayZxz3eu6/K7RpoD3Rx.fkIrVTE0s2bTJ/bZwb2', 'user', 'test', ''),
(5, 'test@test.com', '1212121212', '$2y$10$rfs3Jljm0Cp32NYuBMiQAO.Ik.814mwtfeBADqo1IZAwvT7N3wgjS', 'user', 'test', ''),
(6, 'test@test.com', '1212121212', '$2y$10$Rdj7KFFnJ8Son.S0EM/lqu51XIegsWllD5IUGK3eaR2Qpoi7Ldq12', 'user', 'test', ''),
(7, 'test@mail.com', '123456', '$2y$10$5WA6cm5U2.VdpaAxIUwcneX4/MSjSd/WQ7OWex1euZrI24SPp7HZq', 'test12', 'test12', 'User'),
(8, 'test@test.mail', '00000000', '$2y$10$eIseCD5CDVmQrkY9Pui2cOCAsSp89GacDoHbltbv1cJyFtPCYckdG', 'test_ajout', 'test_autoincrement', 'User');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comID`);

--
-- Index pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD PRIMARY KEY (`favID`);

--
-- Index pour la table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`galID`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`imgID`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tagID`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `comID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `favoris`
--
ALTER TABLE `favoris`
  MODIFY `favID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `galID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `imgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `tagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
